

#include <stdio.h>

//1.determinar si un numero es par o impar
//recibe numero entreo, retorna 1 si es impar o 0 si es par
int par_impar(int num){
    return num%2;
}

//2.fibonachi
//entrada numero entero, salida numero entero
/* el numero que entra se utiliza como  contador, se establecen
 * los casos base, se inicia el bucle while y no para hasta que el 
 * contador sea 1.
 */
int fibonachi(int cont){
    int prim = 0;
    int secu = 1;
    while (cont != 1){
        int num = prim+secu;
        prim = secu;
        secu = num;
        cont--;
    }
    return secu;
}

//3.factorial
//entrada numero entero, salida numero entero
/*se establece el punto de partida y se utiliza un bucle for para 
 * multiplicar desde 2 hasta el numero que queremos 
 */
int factorial(int num){
    int producto = 1;
    for (int i = 2; i <= num; i++){
        producto*=i;
    }
    return producto;
}

//4.largo de un numero con for
//entrada numero entero, salida numero entero
/*establecemos una variable largo en la que se guardara el largo 
 * del numero, como minimo tiene que ser 1, usamos el bucle for 
 * pero de forma infinita, establecemos una variable en la cual 
 * se divide al numero entre 10 para acortarlo y mientras dicha
 * variable no sea 0 no se le suma 1 al largo
 */
int largo_for(int num){
    int largo = 1;
    for (int i = 1; i != 0 ; i++){
        int var = num/10;
        if (var != 0){
            largo+=1;
            num=var;
        }else{
            return largo;
        }
    }
}

//5.largo de un numero con while 
//entrada numero entrero, salida numero entero
/*establecemos una variable largo en la que se guardara el largo 
 * del numero, como minimo tiene que ser 1, usamos el bucle while 
 * pero de forma infinita, establecemos una variable en la cual se
 * divide al numero entre 10 para acortarlo y mientras dicha variable
 * no sea 0 no se le suma 1 al largo
 */
int largo_while(int num){
    int largo = 1;
    while (1){
        int var = num/10;
        if (var != 0){
            largo+=1;
            num=var;
        }else{
            return largo;
        }
    }
}
//6.sumatoria desde 0 hasta n con for
//entrada numero entero, salida numero entero
/*establecemos una variable que va a acumular la suma,
 * usamos un bucle for desde 0 hasta el numero que queremos
 */
int suma_for(int num){
    int acc = 0; 
    for (int i = 0;i <= num;i++){
        acc+=i;
    }
    return acc;
}
//7.sumatoria desde 0 hasta n con while
//entrada numero entero, salida numero entero
/*establecemos una variable que va a acumular la suma y otra que
 * sera la que vamos a sumar, la cual va a ir en aumento, usamos 
 * un bucle while desde 0 hasta el numero que queremos
 */
int suma_while(int num){
    int acc = 0; 
    int i = 0; 
    while (i <= num){
        acc+=i;
        i++;
    }
    return acc;
}

//8.invertir un numero
//entrada numero entero, salida numero entero
/*establecemos tres variables, usamos un bucle while mientra el 
 * numero no sea 0, usamos el %10 para extraer el ultimo digito
 * y lo asignamos a la variable fin, luego /10 para eliminar ese
 * digito del numero y finalmente a la variable inversoo la multiplicamos
 * por 10 y sumamos la variable fin. 
 */
int invertir(int num){
    int fin, res, inverso = 0;
    while (num != 0){
        fin = num%10;
        res = num/10;
        inverso = inverso*10+fin;
        num = res;
    }
    return inverso;
}
//9.determinar si un numero es un palindromo
//recibe numero entero, retorna 1 si no es palindromo o 0 si si es palindromo
/*establecemos tres variables, usamos un bucle while mientra el
 * numero no sea 0, usamos el %10 para extraer el ultimo digito 
 * y lo asignamos a la variable fin, luego /10 para eliminar ese
 * digito del numero y finalmente a la variable inversoo la
 * multiplicamos por 10 y sumamos la variable fin. 
 */
int palindro_num(int num){
    int invertido = invertir(num);
    if (num != invertido){
        return 1;
    }else{
        return 0;
    }
}

int main(){
    
    //1.
    //establecemos la variable a usar
    int num = 22;
    /*si el numero es par mostrara el mensaje es par
     * caso contrario es impar
     */
    if (par_impar(num) == 0){ 
        printf("%d es par\n",num);
    }else{
        printf("%d es impar\n",num);
    }

    //2.
    /*se establece el numero a usar
     * almacenamos el resultado en una variable resul_fibo
     * y luego mostrar un mensaje con el resultado
     */
    int fibo = 6;
    int resul_fibo = fibonachi(fibo);
    printf("el fibonachi de %d es: %d\n",fibo,resul_fibo);

    //3.
    /*creamos la variable que vamos a usar, luego se muestra
     * el mensaje con el numero ingresado y su resultado
     */
    int factor = 10;
    printf("el factorial de %d es: %d\n",factor,factorial(factor));

    //4 y 5.
    /*se establece la variable a usar, y se muestra el mensaje con el numero
     * usado y su resultado para ambos casos en el for y el while 
     */
    int num_1 = 568765845;
    printf("el largo de %d usando for es de %d\n",num_1,largo_for(num_1));
    printf("el largo de %d usando while es de %d\n",num_1,largo_while(num_1));

    //6 y 7.
    /*establecemos la variable a usar y mostramos el mensaje con el 
     * nuemro usado y su resultadotanto para el for como para el whiel 
     */
    int sumador = 5;
    printf("la suma desde 0 hasta %d usando for es de %d\n",sumador,suma_for(sumador));
    printf("la suma desde 0 hasta %d usando while es de %d\n",sumador,suma_while(sumador));


    //8 y 9.
    /*establecemos la variable a utilizar para los ejercicios 8 y 9,
     * en el caso del ejercicio 8 se mustra un mensaje con el numero
     * ingresado y el resultado
     */
    int inver = 121;
    printf("el numero inverso de %d es %d\n",inver,invertir(inver));
     /*en el caso del ejercicio 9 se usa un if en el caso de que la 
      * funcion retornara un 1 mostraria mensaje de que el numero no 
      * es un parindromo caso contrario mostrara mensaje de que si es
      * un palindromo
      */
    if (palindro_num(inver)){
        printf("el numero %d no es un palindromo\n",inver);
    }else{
        printf("el numero %d si es un palindromo\n",inver);
    }

    
    return 0;
}
