#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// 1.eliminar el caracter 'c' de un texto

/*
 * entrada: un puntero "direccion de memoria" con un texto
 *
 * salida: un numero entero
 *
 * comportamiento: usamos strlen para caltular la cantidad de caracteres
 * que aparecen en el arreglo "sin contar el caracter nulo \0"
 */
int largo_arreglo(char *arreglo) {
  int largo = strlen(arreglo);
  return largo;
}

/*
 * entrada: un puntero con un texto y un elemento
 *
 * salida: un numero entero
 *
 * comportamiento: cuenta la cantidad de veces que aparece cualquier caracter
 * menos el elemento
 */
int CantAparicionesSinEl(char *arreglo, char el, int max) {
  int contador = 0;
  // restar 1 a max si se usa scanf o fgets para no contar el \n
  for (int i = 0; i < max; i++) {
    if (arreglo[i] != el) {
      contador += 1;
    }
  }
  return contador;
}

/*
 * entrada: un puntero "direccion de memoria" con un texto
 *
 * salida: un puntero "direccion de memoria" con casi el mismo contenido
 * de lo que entro pero sin el caracter 'c'
 *
 * comportamiento: se calcula la cantidad de apariciones de cualquier
 * caracter menos la 'c', se pide espacio con calloc deacuerdo al calculo
 * anterior, finalmente agregamos todos los caracteres menos las 'c' al
 * espacio de memoria que pedimos y se retorna
 */
char *txt_SinC(char *sms) {
  char el = 'c'; // elemento a eliminar
  int largo_sms = largo_arreglo(sms);
  int apariciones_sin_el = CantAparicionesSinEl(sms, el, largo_sms);
  char *texto = calloc((apariciones_sin_el + 1), sizeof(char));
  int i = 0;
  int j = 0;
  while (apariciones_sin_el > 0) {
    if (sms[i] != el) {
      texto[j] = sms[i];
      i++;
      j++;
      apariciones_sin_el--;
    } else {
      i++;
    }
  }
  return texto;
}

// 2.invertir un texto

/*
 * entrada: un puntero con un texto
 *
 * salida: un puntero con el texto invertido
 *
 * comportamiento: calculamos la cantidad de caracteres del texto que a
 * ingresedo, solicitamos con calloc y el resultado anterior el espacio
 * que ocupamos, invertimos el texto
 */
char *invertir(char *sms) {
  int largo_sms = largo_arreglo(sms);
  char *txt_invertido = calloc((largo_sms + 1), sizeof(char));
  int i = largo_sms - 1;
  int j = 0;
  while (i >= 0) {
    txt_invertido[j] = sms[i];
    i--;
    j++;
  }
  return txt_invertido;
}

// 3.copiar un texto literal a un texto en el heap

/*
 * entrada: un texto literal
 *
 * salida: el mismo texto pero almacenado en el heap
 *
 * comportamiento: se calcula la cantidad de caracteres del texto y pedimos
 * la memoria con calloc, despues copiamos el contenido del texto en el nuevo
 * espacio de memoria
 */
char *copia_heap(char *txt) {
  int largo_txt = largo_arreglo(txt);
  char *copia_txt = calloc((largo_txt + 1), sizeof(char));
  strcpy(copia_txt, txt);
  return copia_txt;
}

int main() {

  // 1.
  /*
   * iniciamos la varibale y solicitamos 100 espacios en la memoria "tomando
   * en cuanta el caracter nulo = \0" del heap con calloc
   */

  char *sms = calloc(100, sizeof(char));
  printf("ingrese un texto con no de mas 99 caracteres: ");
  // scanf no lee espacios pero fgets si
  fgets(sms, 100, stdin);
  /* char *sms = "este texto contiene algunas cuantas letras 'c'"; */
  char *resultado = txt_SinC(sms);
  printf("-------------------------\n");
  printf("el texto sin la letra 'c' seria:\n%s\n", resultado);
  free(resultado);
  printf("-------------------------\n");

  // 2.
  /*
   * nota: se usa el mismo texto que el ejercicio anterior
   */
  char *resultado_2 = invertir(sms);
  printf("el texto invertido seria:\n%s\n", resultado_2);
  free(resultado_2);
  printf("-------------------------\n");

  // 3.
  /*
   * nota: tambien funciona con el texto desde consola
   */
  char *txt = "hola mundo desde el heap";
  char *resultado_3 = copia_heap(txt);
  printf("el texto literal es:\n%s \nel texto del heap es: \n%s\n", txt,
         resultado_3);
  free(resultado_3);

  free(sms); // descomentar si se usa fgets para liberar la memoria
  return 0;
}
