/*
 *Autor: Jesus Gabriel Cordero Díaz
 */
#include "juego.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/*funcion CargarJuego
 *
 * entradas: no hay entradas
 *
 * salida: un puntero del mismo tamaño del numero de las naves con numeros random, que 
 * son las naves que le tocan a cada jugador
 *
 * comportamiento: cremos tres punteros el primero para determinar el simbolo de naves y cual 
 * nave toca a cada jugador, el segundo para la vida de las naves y el terceros para el daño de las
 * naves, usando la funcion AgregarNave agregamos todas las naves que vamos a crear, por ultimo 
 * retornamos un puntero el cual contiene los numeros de las naves de los jugadores
 */
int *CargarJuego(struct Lista *lista_naves) {
    //struct Lista *lista_naves
    int num_naves = 6; 
    int max_X = 0; // maximo  3
    int max_O = 0; // maximo  3
    int pos_jugador1 = 0; // maximo  2
    int pos_jugador2 = 3; // maximo  5
    int *array_naves = NumerosRandom(1, 6, num_naves);
    int *array_vida = NumerosRandom(15, 25, num_naves);
    int *array_daño = NumerosRandom(10, 15, num_naves);
    int *naves_jugadores = calloc(num_naves, sizeof(int));
    for (int i = 0; i < num_naves; i++) {
        // jugador 1 sera las X y el jugador 2 sera O
        if (max_X == 3) {
            AgregarNave(lista_naves, i+1, 'O', array_daño[i], array_vida[i]);
            naves_jugadores[pos_jugador2] = i+1;
            pos_jugador2++;
        }
        else if (max_O == 3) {
            AgregarNave(lista_naves, i+1, 'X', array_daño[i], array_vida[i]);
            naves_jugadores[pos_jugador1] = i+1;
            pos_jugador1++;
        }else {
            // cuando num es par sera jugador 1 y cuando sea impar sera jugador 2
            int num = array_naves[i];
            if ( num%2 == 0) {
                AgregarNave(lista_naves, i+1, 'X', array_daño[i], array_vida[i]);
                naves_jugadores[pos_jugador1] = i+1;
                pos_jugador1++;
                max_X++;
            }else {
                AgregarNave(lista_naves, i+1, 'O', array_daño[i], array_vida[i]);
                naves_jugadores[pos_jugador2] = i+1;
                pos_jugador2++;
                max_O++;
            }
        }
    }
    free(array_naves);
    array_naves = NULL;
    free(array_vida);
    array_vida = NULL;
    free(array_daño);
    array_daño = NULL;
    return naves_jugadores;
}

/*funcion TerminarJuego
 *
 * entradas: la lista de las naves
 *
 * salida: un 0 si aun no hay ganador y un 1 en caso contrario, ademas se imprimira por consola
 * u mensaje que indicara cual jugador gano y cuantas naves  todavia exixten, ademas de mensaje
 * fin del juego
 *
 * comportamiento: creamos dos variables numericas iniciadas en 0 que corresponden al total 
 * naves de cada equipo, recorremos las lista de naves y preguntamos si la nave en la que 
 * estamos es del jugador 1 o del 2 y sumamos al contador correspondiente, depues del
 * recorrer todo la lista preguntamos si algun contador se quedo en 0, de que estar alguno en 0 
 * se termina el juego 
 */
int TerminarJuego(struct Lista *lista_naves) {
    if (lista_naves == NULL) {
        return 1;
    }
    if (lista_naves->inicio == NULL) {
        return 1;
    }
    int acum_X  = 0;
    int acum_O = 0;
    struct Nave *actual = lista_naves->inicio;
    while (actual != NULL) {
        if (actual->simbolo == 'X') {
            acum_X++;
        }else {
            acum_O++;
        }
        actual = actual->sigt;
    }
    if (acum_X == 0) {
        printf("\t\t\t\t\t\t\tJugador 2 gana\n");
        printf("\t\t\t\t\t\t\tFin del juego\n");
        return 1;
    }
    else if (acum_O == 0) {
        printf("\t\t\t\t\t\t\tJugador 1 gana\n");
        printf("\t\t\t\t\t\t\tFin del juego\n");
        return 1;
    }
    return 0;
}

/*funcion NavesDisponibles
 *
 * entradas: la lista de las naves y el jugador en turno
 *
 * salida: no tiene salidas
 *
 * comportamiento: recorremos la lista y mostramos las naves disponibles del jugador en turno
 */
void NavesDisponibles(struct Lista *lista_naves, int turno_jugador) {
    if (lista_naves == NULL) {
        return;
    }
    if (lista_naves->inicio == NULL) {
        return;
    }
    struct Nave *actual = lista_naves->inicio;
    while (actual != NULL) {
        if (turno_jugador == 1 && actual->simbolo == 'X') {
            printf("%d ",actual->indice);
        }
        else if (turno_jugador == 2 && actual->simbolo == 'O') {
            printf("%d ",actual->indice);
        }
    actual = actual->sigt;
    }
    return;
}

/*funcion RealizarAtaque
 *
 * entradas: la lista de las naves y el indice de la nave que ataca
 *
 * salida: no tiene salidas
 *
 * comportamiento: creamos dos punteros el primero para encontrar la nave que ataca y extraer
 * el daño a restar a las otras naves, el segundo para poder restar la vida de las naves en funcion 
 * del daño de la que ataca, ademas se comprueba si la que ataca puede atacar a dos naves "no 
 * importa si son del mismo equipo" o solo puede atacar a una y en que sentido va a atacar
 */
void AtaqueDeNave(struct Lista *lista_naves, int _indice_) {
    if (lista_naves == NULL) {
        return;
    }
    if (lista_naves->inicio == NULL) {
        return;
    }
    struct Nave *atacado, *atacante = lista_naves->inicio;
    // si la que ataca es la primera 
    if (atacante->indice == _indice_) {
        // por si no existen mas naves
        if (atacante->sigt != NULL) {
            int daño = atacante->daño;
            atacado = atacante->sigt;
            atacado->vida -=daño;
        }
        return;
    }
    while (atacante != NULL) {
        if (atacante->indice == _indice_) {
            int daño = atacante->daño;
            atacado->vida -= daño;
            if (atacante->sigt != NULL) {
                atacado = atacante->sigt;
                atacado->vida -= daño;
                return;
            }
            return;
        }
        atacado = atacante;
        atacante = atacante->sigt;
    }
    return;
}

/*funcion NumerosRandom
 *
 * entradas: un valor minimo, un valor maximo y la cantidad de naves del juego
 *
 * salida: un puntero del mismo tamaño del numero de las naves con numeros random
 *
 * comportamiento: creamos un puntero del tamaño de las naves del juego y recorremos 
 * el puntero agregando numeros random, finalmente retornamos el puntero
 */
int *NumerosRandom(int minimo, int maximo, int largo_array) {
    int *array_random = calloc(largo_array, sizeof(int));
    int num = 0;
	srand(time(NULL));
	for (int i = 0; i < largo_array; i++) {
		num = minimo+rand()%((maximo+1)-minimo);
		array_random[i] = num;
	}
    return array_random;
}

/*funcion CrearNave
 *
 * entradas: el indice, el simbolo, el daño y la vida
 *
 * salida: un puntero a la nueva nave
 *
 * comportamiento: creamos una nave, cada nave poseera vida y daño aleatorio, 
 * un simbolo para diferenciarlas ("X" para el jugador_1 y "O" para el jugador_2)
 * y un indice
 */
struct Nave *CrearNave(int _indice_, char _simbolo_, int _daño_, int _vida_) {
    struct Nave *NuevaNave = calloc(1, sizeof(struct Nave));
    NuevaNave->indice = _indice_;
    NuevaNave->simbolo = _simbolo_;
    NuevaNave->daño = _daño_;
    NuevaNave->vida = _vida_;
    return NuevaNave;
}

/*funcion AgregarNave
 *
 * entradas: una lista a la cual se le agregan las "naves" del juego, una cantidad
 * de naves, un arreglo con los numeros de las naves de cada jugador
 *
 * salida: no tiene salidas como tal, pero seria la lista con las 6 naves (en este caso)
 * "las 6 naves con sus debidos prametros"
 *
 * comportamiento: agregamos las naves a la lista
 */
void AgregarNave(struct Lista *lista_naves, int indice, char simbolo, int daño, int vida) {
    if (lista_naves == NULL) {
        return;
    }
    if (lista_naves->inicio == NULL) {
        struct Nave *NuevaNave = CrearNave(indice, simbolo, daño, vida);
        lista_naves->inicio = NuevaNave;
        return;
    }
    else {
        struct Nave *actual = lista_naves->inicio;
        while (actual != NULL) {
            if (actual->sigt == NULL) {
                struct Nave *NuevaNave = CrearNave(indice, simbolo, daño, vida);
                actual->sigt = NuevaNave;
                return;
            }
            else {
                actual = actual->sigt;
            }
        }
    }
}

/*funcion BorrarNaves
 *
 * entradas: una lista de naves
 *
 * salida: no tiene salidas como tal, pero seria la lista de naves mas pequella "aveces"
 *
 * comportamiento: recoremos la lista de naves y preguntamos si la nave que estamos viendo 
 * tiene una vida igual o inferior a 0, en caso de cumplirse la borramos
 */
void BorarNaves(struct Lista *lista_naves) {
    if (lista_naves == NULL) {
        return;
    }
    if (lista_naves->inicio == NULL) {
        return;
    }
    struct Nave *anterior, *borrar = lista_naves->inicio;
    //  por si hay que borrar la primer nave
   if (borrar->vida <= 0) {
       struct Nave *temp = borrar;
        lista_naves->inicio = borrar->sigt;
        free(temp);
        temp = NULL;
   }
   // se reinicia porque sino intenta acceder a memoria que no exixtes
   anterior = lista_naves->inicio;
   borrar = lista_naves->inicio;
   while (borrar != NULL) {
       if (borrar->vida <=0) {
           struct Nave *temp = borrar;
           anterior->sigt = borrar->sigt;
           free(temp);
           temp = NULL;
       }
       anterior = borrar;
       borrar = borrar->sigt;
   }
   return;
}

/*funcion MostrarNaves
 *
 * entradas: una lista de naves
 *
 * salida: no tiene salidas como tal, pero serian todos los valores de las naves (las naves existentes)
 *  "por consola"
 *
 * comportamiento: mostramos todos los datos de cada nave para que el jugar sepa
 * que naves hay disponibles y el estado de las mismas
 */
void MostrarNaves(struct Lista *lista_naves) {
    if (lista_naves == NULL) {
        return;
    }
    if (lista_naves->inicio == NULL) {
        return;
    }
     printf("\t\t\t\t\t\t\t#:   ");
    struct Nave *actual = lista_naves->inicio;
    while (actual != NULL) {
        printf("%d   ",actual->indice); // # = indice o numero de la nave
        actual = actual->sigt;
    }
    printf("\n\t\t\t\t\t\t\tN:  ");
    actual = lista_naves->inicio;
    while (actual != NULL) {
        printf(" %c  ",actual->simbolo); // N = naves o el simbolo
        actual = actual->sigt;
    }
    printf("\n\t\t\t\t\t\t\tD:  ");
    actual = lista_naves->inicio;
    while (actual != NULL) {
        printf("%d  ",actual->daño); // D = daño
        actual = actual->sigt;
    }
    printf("\n\t\t\t\t\t\t\tV:  ");
    actual = lista_naves->inicio;
    while (actual != NULL) {
        if (actual->vida >= 10) {
            printf("%d  ",actual->vida); // V = vida
            actual = actual->sigt;
        }else {
            printf("0%d  ",actual->vida); // V = vida
            actual = actual->sigt;
        }
    }
    printf("\n");
    return;
}