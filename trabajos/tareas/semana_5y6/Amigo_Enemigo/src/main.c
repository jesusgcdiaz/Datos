/*
 *Autor: Jesus Gabriel Cordero Díaz
 */
#include "juego.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
// nota: use pantalla completa

/*funcion main
 *
 * entradas: no tiene entradas
 *
 * salida: no tiene salidas como tal, pero es un 0 para indicar que todo salio bien
 *
 * comportamiento: aqui es donde nuestro juego empieza, habra un bucle principal para 
 * determinie un ganador (el ganador se determina cuando un jugador ya no 
 * tiene mas Naves) y para el funcionamiento completo del juego =)
 */
int main(int argc, char **argv) {
    printf("\t\t\t\tBienvenidos\n");
    system("sleep 2");
    system("clear");
    // iniciamos la lista de naves
    struct Lista *lista_naves = calloc(1, sizeof(struct Lista));
    // iniciamos el juego
    int *naves_jugadores = CargarJuego(lista_naves);
    // se indica que el jugador 1 son las X y el jugador 2 son las O
    printf("\t\t\t\tLas naves de Jugadore 1 son las X y las naves del Jugador 2 son las O\n");
    printf("\t\t\t\t--------------------------------------------------------------------\n");
    // indicamos cuales son las naves con las que comienzan los jugadores
    printf("\t\t\t\t\tLas naves con las que el Jugador 1 comienza son: %d %d %d\n",naves_jugadores[0],naves_jugadores[1],naves_jugadores[2]);
    printf("\t\t\t\t--------------------------------------------------------------------\n");
    printf("\t\t\t\t\tY las naves con las que el Jugador 2 comienza son: %d %d %d\n",naves_jugadores[3],naves_jugadores[4],naves_jugadores[5]);
    // un array de 1 unico elemento para determinar cual jugador empieza 
    int *turno_array = NumerosRandom(1, 2, 1);
    int turno_jugador = turno_array[0];
    // mostramos cual es el jugador en empezar
    printf("\t\t\t\t--------------------------------------------------------------------\n");
    printf("\t\t\t\tEl jugador %d es el primero en atacar\n",turno_jugador);
     printf("\t\t\t\t--------------------------------------------------------------------\n");
    // liberamos la memoria que ya no necesitamos
    free(turno_array);
    turno_array =NULL;
    // inicio de bucle del juego
    while (1) {
        if (TerminarJuego(lista_naves)) {
            return 0;
        }
        MostrarNaves(lista_naves);
        int nave_ataca = 0;
        printf("\t\t\t\t--------------------------------------------------------------------\n");
        printf("Jugador %d tiene disponible  => ",turno_jugador);
        NavesDisponibles(lista_naves, turno_jugador);
        printf(" que nave quiere que ataque: ");
        scanf("%d",&nave_ataca);
        if (turno_jugador == 1 && (nave_ataca == naves_jugadores[0] || nave_ataca == naves_jugadores[1] || nave_ataca == naves_jugadores[2])) {
            AtaqueDeNave(lista_naves, nave_ataca);
            turno_jugador = 2;
        }
        else if (turno_jugador == 2 && (nave_ataca == naves_jugadores[3] || nave_ataca == naves_jugadores[4] || nave_ataca == naves_jugadores[5])) {
            AtaqueDeNave(lista_naves, nave_ataca);
            turno_jugador = 1;
        }else {
            if (turno_jugador == 1) {
                system("clear");
                printf("\t\t\t\t\t\t\tJugador 1 pierde turno\n");
                system("sleep 0.3");
                turno_jugador = 2;
            }else{
                system("clear");
                printf("\t\t\t\t\t\t\tJugador 2 pierde turno\n");
                system("sleep 0.3");
                turno_jugador = 1;
            }
        }
        system("sleep 0.5");
        system("clear");
        BorarNaves(lista_naves);
    }
    return 0;
}