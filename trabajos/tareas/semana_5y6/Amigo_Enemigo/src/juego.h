/*
 *Autor: Jesus Gabriel Cordero Díaz
 */
#ifndef JUEGO_H
#define JUEGO_H

/*struct  Nave
 *
 * entradas: no tiene entradas, pero sus campos son indice, simbolo, daño y vida
 *
 * salida: no tiene salidas como tal, pero seria un puentero a la siguiente Nave, caso
 * contrario apuntara a NULL
 *
 * comportamiento: es un modelo para las Naves del juego las cuales por comodidad
 *  tendran un indice (1-6), un simbolo ("X" o "O"), un daño (10-20) y una vida de (10-30)
 */
struct Nave {
    int indice;
    char simbolo;
    int daño;
    int vida;
    struct Nave *sigt;
};

/*struct  Lista
 *
 * entradas: no tiene entradas, pero este struct no sirve para no perder la lista
 *
 * salida: no tiene salidas como tal, pero seria un puentero a la primer Nave (para
 * no perder nuestra lista), caso contrario apuntara a NULL
 *
 * comportamiento: puntero para no perder nuestra lista y saber donde empezar a buscar
 */
struct Lista {
    struct Nave *inicio;
};

/*funcion TerminarJuego
 *
 * entradas: la lista de las naves
 *
 * salida: un 0 si aun no hay ganador y un 1 en caso contrario, ademas se imprimira por consola
 * u mensaje que indicara cual jugador gano y cuantas naves  todavia exixten, ademas de mensaje
 * fin del juego
 *
 * comportamiento: creamos dos variables numericas iniciadas en 0 que corresponden al total 
 * naves de cada equipo, recorremos las lista de naves y preguntamos si la nave en la que 
 * estamos es del jugador 1 o del 2 y sumamos al contador correspondiente, depues del
 * recorrer todo la lista preguntamos si algun contador se quedo en 0, de que estar alguno en 0 
 * se termina el juego 
 */
int TerminarJuego(struct Lista *lista_naves);

/*funcion CargarJuego
 *
 * entradas: no hay entradas
 *
 * salida: un puntero del mismo tamaño del numero de las naves con numeros random, que 
 * son las naves que le tocan a cada jugador
 *
 * comportamiento: cremos tres punteros el primero para determinar el simbolo de naves y cual 
 * nave toca a cada jugador, el segundo para la vida de las naves y el terceros para el daño de las
 * naves, usando la funcion AgregarNave agregamos todas las naves que vamos a crear, por ultimo 
 * retornamos un puntero el cual contiene los numeros de las naves de los jugadores
 */

//struct Lista *lista_naves
int *CargarJuego(struct Lista *lista_naves);

/*funcion AtaqueDeNave
 *
 * entradas: la lista de las naves y el indice de la nave que ataca
 *
 * salida: no tiene salidas
 *
 * comportamiento: creamos dos punteros el primero para encontrar la nave que ataca y extraer
 * el daño a restar a las otras naves, el segundo para poder restar la vida de las naves en funcion 
 * del daño de la que ataca, ademas se comprueba si la que ataca puede atacar a dos naves "no 
 * importa si son del mismo equipo" o solo puede atacar a una y en que sentido va a atacar
 */
void AtaqueDeNave(struct Lista *lista_naves, int _indice_);

/*funcion NavesDisponibles
 *
 * entradas: la lista de las naves y el jugador en turno
 *
 * salida: no tiene salidas
 *
 * comportamiento: recorremos la lista y mostramos las naves disponibles del jugador en turno
 */
void NavesDisponibles(struct Lista *lista_naves, int turno_jugador);

/*funcion NumerosRandom
 *
 * entradas: un valor minimo, un valor maximo y la cantidad de naves del juego
 *
 * salida: un puntero del mismo tamaño del numero de las naves con numeros random
 *
 * comportamiento: creamos un puntero del tamaño de las naves del juego y recorremos 
 * el puntero agregando numeros random, finalmente retornamos el puntero
 */
int *NumerosRandom(int minimo, int maximo, int largo_array) ;

/*funcion CrearNave
 *
 * entradas: el indice, el simbolo, el daño y la vida
 *
 * salida: un puntero a la nueva nave
 *
 * comportamiento: creamos una nave, cada nave poseera vida y daño aleatorio, 
 * un simbolo para diferenciarlas ("X" para el jugador_1 y "O" para el jugador_2)
 * y un indice
 */
struct Nave *CrearNave(int _indice_, char _simbolo_, int _daño_, int _vida_);

/*funcion AgregarNave
 *
 * entradas: una lista a la cual se le agregan las "naves" del juego, un indice, un 
 * simbolo, un daño y una vida
 *
 * salida: no tiene salidas como tal, pero seria la lista con las 6 naves (en este caso)
 * "las 6 naves con sus debidos prametros"
 *
 * comportamiento: agregamos la nave a la lista
 */
void AgregarNave(struct Lista *lista_naves, int indice, char simbolo, int daño, int vida);

/*funcion BorrarNaves
 *
 * entradas: una lista de naves
 *
 * salida: no tiene salidas como tal, pero seria la lista de naves mas pequella "aveces"
 *
 * comportamiento: recoremos la lista de naves y preguntamos si la nave que estamos viendo 
 * tiene una vida igual o inferior a 0, en caso de cumplirse la borramos
 */
void BorarNaves(struct Lista *lista_naves) ;

/*funcion MostrarNaves
 *
 * entradas: una lista de naves
 *
 * salida: no tiene salidas como tal, pero serian todos los valores de las naves (las naves existentes)
 *  "por consola"
 *
 * comportamiento: mostramos todos los datos de cada nave para que el jugar sepa
 * que naves hay disponibles y el estado de las mismas
 */
void MostrarNaves(struct Lista *lista_naves);

#endif
