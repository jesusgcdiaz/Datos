#comando a ejecutar: gcc -lm -std=c11 -g <archivo(s)> -o <nombre del ejecutable>

#declaracion de constantes y variables

#constantes
naruto = -lm -std=c11 -g 
salida = ejecutable
ls = ls
ruta_actual = ./
espacio = "------------------------------------"

#colores
rojo="\033[0;31m"
verde="\033[0;32m"
amarillo="\033[0;33m"
azul="\033[0;34m"
purpura="\033[0;35m"
cian="\033[0;36m"

#tipo de letra
negrita="\033[1m"
cursiva="\033[3m"
normal="\033[0m"

#variables
archivos = main.c


#reglas que se ejecutan por defecto
all:
	@echo -e $(amarillo)$(negrita)$(cursiva)"creando archivo ==> $(salida)... "
	@gcc $(naruto) $(archivos) -o $(salida)
	@echo -e $(cian)$(negrita)$(espacio)
	@echo -e $(purpura)$(negrita)$(cursiva)"archivo $(salida) creado con exito!"
	@echo -e $(cian)$(negrita)$(espacio)
	@ls | grep --color=auto $(salida)

#para ejecutar 
run:
	@echo -e $(purpura)$(negrita)$(cursiva) "ejecutando el archivo => $(salida)"
	@echo -e $(cian)$(negrita)$(espacio)
	@echo -e $(normal) ""
	@./$(salida)
	@echo -e $(normal) ""
	@echo -e $(cian)$(negrita)$(espacio)

#borrar y recompilar
recompilar:
	@exa --group-directories-first
	@echo -e $(cian)$(negrita)$(espacio)
	@echo -e $(amarillo)$(negrita)$(cursiva) "recompilando => $(salida)..."
	@echo -e $(cian)$(negrita)$(espacio)
	@rm $(salida)
	@gcc $(naruto) $(archivos) -o $(salida)
	@echo -e $(verde)$(negrita)$(cursiva) "archivo recompilando ==> $(salida)"
	@echo -e $(cian)$(negrita)$(espacio)
	@ls | grep --color=auto $(salida)
	


#remover el archivo ejecutable
.PHONY clean:
	@echo -e $(verde) "\c"
	@exa --group-directories-first
	@echo -e $(cian)$(negrita)$(espacio)
	@echo -e $(negrita)$(rojo) "\c"
	@rm -i $(salida)
	@echo -e $(cian)$(negrita)$(espacio)
	@exa --group-directories-first
