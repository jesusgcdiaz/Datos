/*
* Autor: Jesús GC Díaz
* Version: 0.0.4
*/
#ifndef listas_H


#define listas_H

/*struct  nodo
*
* entradas: no tiene entradas como tal, pero seria un numero entero
*
* salida: no tiene salidas como tal, pero seria un puntero al siguiente nodo,
* caso contrario apunta a NULL
*
* comportamiento: es una plantilla para poder crear un nodo, sigt es un puntero que 
* apuntaria al siguiente nodo, en el caso de ser el ultimo nodo este apuntara a NULL
*/
struct nodo {
    int valor;
    struct nodo *sigt;
};

/*struct  nodo
*
* entradas: no tiene entradas
*
* salida: no tiene salidas como tal, pero seria un puntero al siguiente nodo,
* caso contrario apunta a NULL
*
* comportamiento: es la cabeza de la lista, sin este puntero nunca sabremos 
* donde empieza nuestra lista y por consecuente la perderemos y desperdiciamos
* memoria de heap
*/
struct Lista {
    struct nodo *inicio;
};


/* funcion CrearNodo
*
* entrada: un numero entero para agregar al nodo
*
* salida: un puentero al nodo que acabamos de crear
*
* comportamiento: creamos  un NuevoNodo con la pantilla nodo, con el operador flecha 
* asignamos a valor el num y retornamos la direccion de memoria del nodo
*/
struct nodo *CrearNodo(int num);

/* funcion InsetarEnLista
*
* entrada: una lista y un numero entero a insertar
*
* salida: no se retorna nada
*
* comportamiento: se recorre la lista hasta encontar el lugar en donde vamos
* a insertar el numero, creamos el nodo y lo enlazamos a la lista que nos
* dieron
*
* NOTA: esta funcion no la diseñe/pense yo, su autor es el Profe Aurelio
* Sanabria del Tec
*/
void InsetarEnLista(struct Lista *lista, int num);

/* funcion BorrarElemento
*
* entrada: una lista y un elemento a eliminar
*
* salida: no tiene salida
* comportamiento: recorro la lista enlazada hasta encontrar el elemento que se
* quiere eliminar, guardamos la direccion de memoria del nodo que vamos a 
* eliminar para liberarla, hacemos que el nodo que esta antes del nodo que 
* vamos a eliminar apunte al nodo que esta despues del nodo a eliminar y 
* por ultimo eliminamos el nodo que se quiere borrar
*/
void BorrarElemento(struct Lista *lista, int el);

/* funcion BuscarElemento
*
* entrada: una lista y un elemento a buscar
*
* salida: el numero del nodo en el cual se encuentra el elemento
*
* comportamiento: recorremos la lista buscando el elemento que se nos paso
* por paramentro, cuando los encontramos mostramos por consola la posicion
* del nodo que contiene el elemento
*/
void BuscarElemento(struct Lista *lista, int el);

/* funcion MostarLista
*
* entrada: una lista
*
* salida: los elementos de la lista "por consola"
*
* comportamiento: recorro la lista enlazada e imprimo por consola los elementos
* hasta legar al ultimo elemento el cual apunta a NULL
*/
void MostarLista(struct Lista *lista);

#endif
