/*
 * Autor: Jesús GC Díaz
 * Version: 0.0.4
 */
#include "listas.h"
#include <stdio.h>
#include <stdlib.h>

/* funcion CrearNodo
 *
 * entrada: un numero entero para agregar al nodo
 *
 * salida: un puentero al nodo que acabamos de crear
 *
 * comportamiento: creamos  un NuevoNodo con la pantilla nodo, con el operador
 * flecha asignamos a valor el num y retornamos la direccion de memoria del nodo
 */
struct nodo *CrearNodo(int num) {
  struct nodo *NuevoNodo = calloc(1, sizeof(struct nodo));
  NuevoNodo->valor = num;
  return NuevoNodo;
}

/* funcion InsetarEnLista
 *
 * entrada: una lista y un numero entero a insertar
 *
 * salida: no se retorna nada
 *
 * comportamiento: se recorre la lista hasta encontar el lugar en donde vamos
 * a insertar el numero, creamos el nodo y lo enlazamos a la lista que nos
 * dieron
 *
 * NOTA: esta funcion no la diseñe/pense yo, su autor es el Profe Aurelio
 * Sanabria del Tec
 */
void InsetarEnLista(struct Lista *lista, int num) {
  if (lista == NULL) {
    return;
  } else if (lista->inicio == NULL) {
    struct nodo *NuevoNodo = CrearNodo(num);
    lista->inicio = NuevoNodo;
  } else {
    struct nodo *actual = lista->inicio;
    while (actual != NULL) {
      if (actual->sigt == NULL) {
        struct nodo *NuevoNodo = CrearNodo(num);
        actual->sigt = NuevoNodo;
        // return;
        break;
      } else {
        actual = actual->sigt;
      }
    }
  }
  return;
}

/* funcion BorrarElemento
 *
 * entrada: una lista y un elemento a eliminar
 *
 * salida: no tiene salida
 *
 * comportamiento: recorro la lista enlazada hasta encontrar el elemento que se
 * quiere eliminar, guardamos la direccion de memoria del nodo que vamos a
 * eliminar para liberarla, hacemos que el nodo que esta antes del nodo que
 * vamos a eliminar apunte al nodo que esta despues del nodo a eliminar y
 * por ultimo eliminamos el nodo que se quiere borrar
 */
void BorrarElemento(struct Lista *lista, int el) {
  if (lista == NULL) {
    return;
  }
  if (lista->inicio == NULL) {
    return;
  }
  struct nodo *actual, *borar = lista->inicio;
  // validacion por si el elemento que vamos a borrar es el primero
  // es que hace cosas muy locas en el bucle
  if (borar->valor == el) {
    struct nodo *temp = borar;
    lista->inicio = borar->sigt;
    free(temp);
    return;
  }
  while (borar != NULL) {
    if (borar->valor == el) {
      struct nodo *temp = borar;
      actual->sigt = borar->sigt;
      free(temp);
      // return;
      // break;
    }
    actual = borar;
    borar = borar->sigt;
  }
}

/* funcion BuscarElemento
 *
 * entrada: una lista y un elemento a buscar
 *
 * salida: el numero del nodo en el cual se encuentra el elemento
 *
 * comportamiento: recorremos la lista buscando el elemento que se nos paso
 * por paramentro, cuando los encontramos mostramos por consola la posicion
 * del nodo que contiene el elemento
 */
void BuscarElemento(struct Lista *lista, int el) {
  if (lista == NULL) {
    return;
  }
  if (lista->inicio == NULL) {
    return;
  }
  int acum = 0;
  struct nodo *actual = lista->inicio;
  while (actual != NULL) {
    if (actual->valor == el) {
      printf("el elemento se encuentra en el nodo %d\n", acum);
      return;
    }
    acum++;
    actual = actual->sigt;
  }
}

/* funcion MostarLista
 *
 * entrada: una lista
 *
 * salida: los elementos de la lista "por consola"
 *
 * comportamiento: recorro la lista enlazada e imprimo por consola los elementos
 * hasta legar al ultimo elemento el cual apunta a NULL
 */
void MostarLista(struct Lista *lista) {
  if (lista == NULL) {
    return;
  }
  struct nodo *actual = lista->inicio;
  while (actual != NULL) {
    printf("%d -> ", actual->valor);
    actual = actual->sigt;
  }
  printf("NULL\n");
}
