/*
 * Autor: Jesús GC Díaz
 * Version: 0.0.4
 */
#include "listas.h"
#include <stdio.h>
#include <stdlib.h>

/* duda al profe, los estruct tiene que ir el el .h ???, es que al compilar
me daba un error y decia que era en el sizeof */

/*
 * entrada: no posee entradas
 *
 * salida: no retorna nada
 *
 * comportamiento: se usa la biblioteca listas.h para InsertarEnLista, CrearNodo
 * BorrarNodo, etc
 */
int main(int argc, char **argv) {
  // se establece en 0 para que no se llene de basura
  int valor = 0;
  printf("ingrece un valor para agregar a la lista: ");
  scanf("%d", &valor);
  // creamos la lista vacia
  struct Lista *lista_1 = calloc(1, sizeof(struct Lista));
  while (valor >= 0) {
    // insertamos al final de la lista
    InsetarEnLista(lista_1, valor);
    // MostarLista(lista_1);
    printf("ingrece otro valor para agregar a la lista: ");
    scanf("%d", &valor);
  }
  // mostramos la lista sin cambios
  printf("lista sin cambios\n");
  MostarLista(lista_1);
  // se establece en 0 para que no se llene de basura
  int el = 0;
  printf("ingrese un elemento a borar: ");
  scanf("%d", &el);
  // borrara solo el primer elemento que encuentre si hay mas no los quita
  BorrarElemento(lista_1, el);
  // se muestra la lista con el elemento borrado
  MostarLista(lista_1);
  // se establece en 0 para que no se llene de basura
  int buscar = 0;
  printf("ingrese un elemento a buscar: ");
  scanf("%d", &buscar);
  BuscarElemento(lista_1, buscar);
  return 0;
}
