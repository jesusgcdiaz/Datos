# Repositorios con los trabajos de Estructura de Datos

***Idioma***
-  Español

### Para compilar los archivos hay dos formas utilizar el makefile o de forma manual
```bash
# Makefile:

# para compilar la primera vez
make 
# ejecutar el archivo ejecutable
make run 
# borrar y recompilar el ejecutable
make recompilar 
# borrar el ejecutable
make clean

# Manualmente:

# compilar todos los .c y .h
gcc -lm -std=c11 -g  *.c *.h  -o ejecutable.out 
# ejecutar el archivo ejecutable 
./ejecutable.out 
```

### Usando gdb para depurar los archivos .c
```bash
# para iniciar el gdb con el archivo ejecutable
gdb ./<ejecutable.out>
# colocar un punto de interrupción
break <NombreDeLaFunción>
# empezar la ejecución hasta llegar al punto de interrupción
run
# mostrar una parte del codigo
list
# para pasar a la siguiente instrucción
next
# imprimir valores de variables o comprobar datos
print <variable>
# mostrar siempre el valor de una variable
display <variable>
```

## Asignaciones

| Semana        | Debe resolver                                   |
| ------------- | ----------------------------------------------- |
| **Semana_1**  | Ninguna asignación                              |


| Semana        | Debe resolver                                   |
| ------------- | ----------------------------------------------- |
| **Semana_2**  | Determinar si un numero es par o impar          |
| **Semana_2**  | Fibonachi                                       |
| **Semana_2**  | Factorial                                       |
| **Semana_2**  | Largo de un numero con for                      |
| **Semana_2**  | Largo de un numero con while                    |
| **Semana_2**  | Sumatoria desde 0 hasta n usando for            |
| **Semana_2**  | Sumatoria desde 0 hasta n usando while          |
| **Semana_2**  | Invertir un numero                              |
| **Semana_2**  | Invertir un numero                              |
| **Semana_2**  | Determinar si un numero es palindromo           |


| Semana        | Debe resolver                                   |
| ------------- | ----------------------------------------------- |
| **Semana_3**  | Eliminar el cararter 'c' de un texto            |
| **Semana_3**  | Invertir un texto                               |
| **Semana_3**  | Copiar un texto literal a un texto en el heap   |

| Semana        | Debe resolver                                   |
| ------------- | ----------------------------------------------- |
| **Semana_5**  | Trabajo de Amigo_Enemigo       |

| Semana        | Debe resolver                                   |
| ------------- | ----------------------------------------------- |
| **Semana_6**  | Continuar con el trabajo de semana_5      |

```
Autor ==> JesúsGCDíaz 
Fecha ==> 10/03/21
```
